/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Modelo;

import Util.Cola;
import Util.Pila;

/**
 *
 * @author pc06
 */
public class MatrizCola {
    private Cola<Integer> [] matrix;
    
    public MatrizCola(int [][]m){
        if(m == null)
            throw new RuntimeException(":(");
        
        int filas = m.length;
        int colum = m[0].length;
        this.matrix = new Cola[filas];
        for(int i = 0; i < filas; i++)
            matrix[i] = new Cola();
        
        for(int i = 0; i < m.length; i++){
            for(int j = 0; j < m[i].length; j++){
                matrix[i].enColar(m[i][j]);
            }
        }
    }
    
    public int get(int i, int j){
        if(this.matrix == null)
            throw new RuntimeException("La matriz es vacía.");
        
        this.validarPosiciones(i, j);
        
        int datoBuscado = 0;
        
        for(int k = 0; k < matrix[i].size(); k++){
            //sacar el dato y pasarlo de nuevo
            int dato = matrix[i].deColar();
            matrix[i].enColar(dato);
            
            //ver si lo encontramos
            if(k == j){
                datoBuscado = dato;
            }            
        }
        return datoBuscado;
    }
    
    public void set(int i, int j, int nuevoDato){
        if(this.matrix == null)
            throw new RuntimeException("La matriz es vacía.");
        
        this.validarPosiciones(i, j);
        
        for(int k = 0; k < matrix[i].size(); k++){
            //sacar el dato 
            int dato = matrix[i].deColar();
            //verificar la ubicacion dentro de la cola (j) y cambiar de haberla encontrado
            if(k == j){
                dato = nuevoDato;
            } 
            //pasar de nuevo el dato
            matrix[i].enColar(dato);
        }
    }
    
    public Cola<Integer> getMenores(){
        if(this.matrix == null)
            throw new RuntimeException("La matriz es vacía.");
        
        Cola<Integer> colaMenores = new Cola();
        int datoMenor = this.getDatoMenor();
        
        //recorrer todas las colas
        for(int i = 0; i < this.matrix.length; i++){
            //mirar todos los elementos de la cola
            for(int j = 0; j < this.matrix[i].size(); j++){
                //sacar el dato 
                int dato = matrix[i].deColar();
                //Comprobar si el dato es menor para almacenarlo en colaMenores
                if(dato == datoMenor){
                    colaMenores.enColar(dato);
                } 
                //pasar de nuevo el dato
                matrix[i].enColar(dato);
            }
        }
        
        return colaMenores;
    }
    
    private int getDatoMenor() {
        int datoMenor = Integer.MAX_VALUE;
        
        //recorrer todas las colas
        for(int i = 0; i < this.matrix.length; i++){
            //mirar todos los elementos de la cola
            for(int j = 0; j < this.matrix[i].size(); j++){
                //sacar el dato 
                int dato = matrix[i].deColar();
                //Comprobar si el dato es menor para cambiar el valor
                if(dato < datoMenor){
                    datoMenor = dato;
                } 
                //pasar de nuevo el dato
                matrix[i].enColar(dato);
            }
        }
        
        return datoMenor;
    }

    @Override
    public String toString() {
        if(this.matrix == null)
            throw new RuntimeException("La matriz es vacía.");
        
        String msg = "\n";
        for(int i = 0; i < matrix.length; i++){
            
            //decolar un dato, manipularlo, y volver a encolar. 
            //Se recorre toda la cola para dejarla como estaba.
            for(int j = 0; j < matrix[i].size(); j++){
                int dato = matrix[i].deColar();
                msg += dato + "\t";
                matrix[i].enColar(dato);
            }
            
            //agregamos un salto de linea para hacerlo de forma rectangular
            msg += "\n";
        }
        return msg;
    }

    private void validarPosiciones(int i, int j) {
        if(this.matrix.length <= i || i < 0)
            throw new RuntimeException("La posición i no es válida");
        if(this.matrix[i].size() <= j || j < 0)
            throw new RuntimeException("La posición j no es válida");
    }

    
}
