/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Modelo;

import Util.*;

/**
 *
 * @author Docente
 */
public class ListadoFracciones {
    
    private ListaCD<Fraccionario> fracciones=new ListaCD();

    public ListadoFracciones(String url) {
        
        
        ArchivoLeerURL archivo=new ArchivoLeerURL(url);
        Object datos[]=archivo.leerArchivo();
        for (int i = 1; i < datos.length; i++) {
            String fila = datos[i].toString();
            String data[]=fila.split(",");
            float num=Float.parseFloat(data[0]);
            float den=Float.parseFloat(data[1]);
            this.fracciones.insertarFin(new Fraccionario(num, den));
            
        }
    }

    

    @Override
    public String toString() {

        String msg = "";
        for (int i = 0; i < this.fracciones.getSize(); i++) {
            Fraccionario f = this.fracciones.get(i);
            msg += f.toString() + "\n";

        }
        return msg;
    }

    public void sort()
    {
        this.fracciones.sort();
    }
}
