/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Vista;

import Negocio.SIA;

/**
 *
 * @author profesor
 */
public class TestLeerURL_SIA {

    public static void main(String[] args) {
        try {
            String url = "https://gitlab.com/madarme/archivos-persistencia/-/raw/master/estudiantes2024.csv";
            SIA sia = new SIA(url);
            System.out.println(sia.getListado((byte) 4));
        }catch(Exception e){
            System.out.println(e.getMessage());
        }

    }
}
