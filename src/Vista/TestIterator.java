/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Vista;

import Util.ListaCD;
import java.util.Random;

/**
 *
 * @author profesor
 */
public class TestIterator {

    public static void main(String[] args) {
        int n=500000;
        escenario1(n);
        escenario2(n);
    }

    
    
    
    public static void escenario1(int n)
    {
    
        ListaCD<Integer> l = random(n);
        long inicio=System.currentTimeMillis();
        System.out.println("*********************************************");
        System.out.println("Escenario 1:");
        System.out.println("Suma:"+suma(l));
        long fin=System.currentTimeMillis();
        fin=fin-inicio;
        System.out.println("Se demoro:"+(fin/1000)+" seg");
    }
    
    
     public static void escenario2(int n)
    {
    
        ListaCD<Integer> l = random(n);
        long inicio=System.currentTimeMillis();
        System.out.println("*********************************************");
        System.out.println("Escenario 2:");
        System.out.println("Suma:"+suma2(l));
        long fin=System.currentTimeMillis();
        fin=fin-inicio;
        System.out.println("Se demoro:"+(fin/1000)+" seg");
    }
    
    
    
    
    private static ListaCD<Integer> random(int n) {
        ListaCD<Integer> l = new ListaCD();
        while (n > 0) {
            //l.insertarInicio(new Random().nextInt());
            l.insertarInicio(1);
            n--;
        }
        return l;
    }

    private static long suma(ListaCD<Integer> l) {
        long t = 0;
        for (int i = 0; i < l.getSize(); i++) {
            t += l.get(i); //<--lento
        }
        return t;
    }
    
    
    private static long suma2(ListaCD<Integer> l) {
        long t = 0;
        for (Integer dato:l) { //--> hastaNext()
            t += dato; //<-- :) --> next() 
        }
        return t;
    }
}
