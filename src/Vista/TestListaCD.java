/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Vista;

import Util.ListaCD;

/**
 *
 * @author docente
 */
public class TestListaCD {

    public static void main(String[] args) {
        ListaCD<Integer> l1 = new ListaCD();
        ListaCD<Integer> l2 = new ListaCD();
        for (int i = 0; i < 10; i++) {
            l1.insertarFin(i);
            l2.insertarInicio(i);
        }
        System.out.println("Lista1:"+l1);
        System.out.println("Lista2:"+l2);
        System.out.println("borrando de l1 la pos=2 el dato es:"+l1.remove(2));
        System.out.println("borrando de l2 la pos=2 el dato es:"+l2.remove(2));
        System.out.println("Las listas quedan:\n"+l1+"\n"+l2);
        
    }

}
