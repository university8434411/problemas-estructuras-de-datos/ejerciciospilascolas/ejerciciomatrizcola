/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Vista;

import Modelo.MatrizCola;

/**
 *
 * @author pc06
 */
public class TestMatrizCola {
    public static void main(String[] args) {
        int [][] m = new int[5][4];
        
        //dejaremos la ultima fila para llenarla de datos menores
        for(int i = 0; i < 4; i++){
            for(int j = 0; j < 4; j++){
                m[i][j] = i + j;
            }
        }
        
        //llenar la fila [4] con minimos para test de getMenores
        for(int j = 0; j < 4; j++){
            m[4][j] = 0;
        }
        
        MatrizCola matrix = new MatrizCola(m);
        
        println("Imprimiendo matriz normal:");
        for(int i = 0; i < 5; i++){
            for(int j = 0; j < 4; j++){
                print(m[i][j] + "\t");
            }
            println();
        }
        println();
        
        println("Imprimiendo matriz de colas:");
        println(matrix);
        
        println("Buscando el elemento en al ubicacion [2][2]: ");
        println(matrix.get(2, 2));
        
        println("Imprimiendo matriz de colas tras buscar:");
        println(matrix);
        
        println("Cambiando valores en ubicación [1][2] y [3][1] por 100 y 25000 respectivamente.");
        matrix.set(1, 2, 100);
        matrix.set(3, 1, 25000);
        println(matrix);
        
        println("Llamando método getMenores():");
        println(matrix.getMenores());
        
    }
    
    public static void print(Object o){
        System.out.print(o);
    }
    
    public static void println(Object o){
        System.out.println(o);
    }
    
    //para un new line
    public static void println(){
        System.out.println();
    }
}
