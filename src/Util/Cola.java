/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Util;

/**
 *
 * @author docente
 */
public class Cola<T extends Comparable> {
    //decorator:
    private ListaCD<T> tope = new ListaCD();

    public Cola() {
    }

    public void enColar(T info) {
        this.tope.insertarFin(info);
    }

    public T deColar() {
        return this.tope.remove(0);
    }

    public boolean isVacia() {
        return this.tope.isVacia();
    }

    public int size()
    {
        return this.tope.getSize();
    }
    
    @Override
    public String toString() {
        return tope.toString();
    }
}
