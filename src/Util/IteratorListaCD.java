/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Util;

import java.util.Iterator;

/**
 *
 * @author profesor
 * @param <T>
 */
public class IteratorListaCD<T> implements Iterator<T> {

    private NodoD<T> centinela, actual;

    public IteratorListaCD(NodoD<T> centinela) {
        this.centinela = centinela;
        this.actual = this.centinela.getSig();
    }

    public boolean hasNext() {
        return this.actual != this.centinela;
    }

    @Override
    public T next() {

        if (!hasNext()) {
            throw new RuntimeException("no hay más datos a iterar");
        }

        T info = this.actual.getInfo();
        this.actual = this.actual.getSig();
        return info;
    }

}
